FROM node:8-alpine

WORKDIR /usr/app

COPY package.json .

RUN npm i --quiet

COPY . .
EXPOSE 5000
RUN npm install pm2 -g

CMD ["pm2-runtime", "index.js"]