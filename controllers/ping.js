exports.ping_get = (req , res)=>{
    res.status(201).json({
        "status":"success",
        "version":`NODE API VERSION ${process.env.API_VERSION}`
    });
}

exports.ping_post = (req , res)=>{
    console.log("Request" , req.body);
    console.log("Request" , req.auth);
    res.status(201).json({
        "status":"success",
        "version":`NODE API VERSION ${process.env.API_VERSION}` ,
        "testMessage": `${req.body.testMessage}`
    });
}   