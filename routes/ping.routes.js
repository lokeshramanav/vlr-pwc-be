const express = require("express");
const router = express.Router();
const expressJwt = require('express-jwt');

const pingController = require("../controllers/ping");

const requireSignin = expressJwt({
    secret: process.env.JWT_SECRET,
    requestProperty: 'auth',
    algorithms: ['HS256']
});

router.get( "/ping"  , pingController.ping_get )
router.post( "/ping" , pingController.ping_post )
router.post( "/ping-jwt" ,requireSignin, pingController.ping_post )

module.exports = router;