require("dotenv").config();

const express = require("express");
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const errorHandler = require("./middlewares/error");
//DB
const connectDB = require('./models/db');
connectDB();


//MIDDLEWARES
app.use(express.json());
app.use(morgan('combined'))
app.use(cors({origin: true, credentials: true}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "x-access-token, Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//ROUTES
app.use('/api' , require('./routes/ping.routes'))
app.use('/api/auth' , require('./routes/auth.routes'))

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(PORT, () =>
  console.log(`Sever running on port ${PORT}`)
);

process.on("unhandledRejection", (err, promise) => {
    console.log(`Logged Error: ${err.message}`);
    server.close(() => process.exit(1));
});
  